#![allow(warnings)]
#![allow(unused)]
#![allow(dead_code)]
#![recursion_limit = "256"]
use darling::FromDeriveInput;

use proc_macro::{self, TokenStream};
use quote::quote;
use syn::punctuated::Punctuated;
use syn::{parse_macro_input, Attribute, Token};

#[derive(FromDeriveInput, Default)]
#[darling(default, attributes(mqtt_data))]
struct Opts {
    topic: Option<String>,
    act: Option<String>,
    typ: Option<String>,
    description: Option<String>,
}
fn get_mqtt_data(attrs: &[Attribute], key: &str) -> String {
    let resv = attrs
        .iter()
        .filter_map(|attr: &syn::Attribute| {
            let mut res = "".to_owned();
            if attr.path().is_ident("mqtt_data") {
                let nested = attr
                    .parse_args_with(Punctuated::<syn::Meta, Token![,]>::parse_terminated)
                    .ok()?;
                for meta in nested {
                    match meta {
                        syn::Meta::NameValue(nv) => {
                            if nv.path.is_ident(key) {
                                match nv.value {
                                    syn::Expr::Lit(syn::ExprLit { attrs: s, lit }) => {
                                        if let syn::Lit::Str(lit) = lit {
                                            res = lit.value();
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }
            if res == "" {
                None
            } else {
                Some(res)
            }
        })
        .collect::<Vec<_>>();
    if resv.len() > 0 {
        resv.first().unwrap().to_owned()
    } else {
        "".to_owned()
    }
}

#[proc_macro_derive(MqttData, attributes(mqtt_data))]
pub fn mqttmsg_derive(input: TokenStream) -> TokenStream {
    let ast: syn::DeriveInput = syn::parse(input.clone()).unwrap();
    let input = parse_macro_input!(input);
    let opts = Opts::from_derive_input(&input).expect("Wrong options");
    let syn::DeriveInput { ident, .. } = input;

    let topic_s = opts.topic.unwrap_or("".to_owned());
    let act_s = opts.act.unwrap_or("".to_owned());
    let typ_s = opts.typ.unwrap_or("".to_owned());
    let description_s = opts.description.unwrap_or("".to_owned());
    let topic_s2 = topic_s.clone();
    let act_s2 = act_s.clone();
    let typ_s2 = typ_s.clone();
    let typ_s3 = typ_s.clone();

    let name = ast.ident.clone();
    let name2 = ast.ident.to_string();
    let iden2 = ast.ident.clone();
    let mut isEnum = false;
    let fields = match &ast.data {
        syn::Data::Struct(s) => &s.fields,
        syn::Data::Enum(e) => {isEnum = true; &syn::Fields::Unit},
        _ => panic!("MqttData can only be derived for structs"),
    };
    let examplev = fields.iter().map(|f| get_mqtt_data(&f.attrs, "example"));
    let examplev2 = examplev.clone();
    let examplev3 = examplev.clone();
    let examplev4 = examplev.clone();
    let field_types = fields.iter().map(|f| &f.ty);
    let field_types2 = field_types.clone();
    let field_types3 = field_types.clone();
    let field_types4 = field_types.clone();
    let field_types5 = field_types.clone();
    let descriptionv = fields
        .iter()
        .map(|f| get_mqtt_data(&f.attrs, "description"));
    let descriptionv2 = descriptionv.clone();
    let descriptionv3 = descriptionv.clone();
    let descriptionv4 = descriptionv.clone();
    let descriptionv5 = descriptionv.clone();
    let descriptionv6 = descriptionv.clone();
    let field_names11 = fields.iter().map(|f| f.ident.as_ref().unwrap().to_string());
    let field_names12 = fields.iter().map(|f| f.ident.as_ref().unwrap());
    let field_names21 = field_names11.clone();
    let field_names22 = field_names12.clone();
    let field_names31 = field_names11.clone();
    let field_names32 = field_names12.clone();
    let field_names42 = field_names12.clone();
    let field_names52 = field_names12.clone();
    let for_struct_gen = quote! {
        impl XValue for #iden2 {
            fn create_propertie(typ: String, ex_value: String, description: String) -> ReferenceOr<Box<Schema>> {
                ReferenceOr::Item(Box::new(asyncapi::Schema {
                    schema_data: asyncapi::SchemaData {
                        ..Default::default()
                    },
                    schema_kind: asyncapi::SchemaKind::Type(asyncapi::schema::Type::Object(
                        ObjectType {
                            properties: IndexMap::from([
                                #((
                                    (#field_names31).to_owned(),
                                    #field_types2::create_propertie(stringify!(#field_types2).to_owned(), #examplev2.to_owned(), #descriptionv2.to_owned())
                                )),*
                            ]),
                            ..Default::default()
                        },
                    )),
                }))
            }
        }
    };
    let for_enum_gen = quote! {
        impl XValue for #iden2 {
            fn create_propertie(typ: String, ex_value: String, description: String) -> ReferenceOr<Box<Schema>> {
                let mut schema = Schema {
                    schema_kind: asyncapi::SchemaKind::Type(asyncapi::schema::Type::Integer(IntegerType {
                        format: VariantOrUnknownOrEmpty::Item(IntegerFormat::Int32),
                        enumeration: Vec::new(),
                        ..Default::default()
                    })),
                    schema_data: SchemaData {
                        description: Some(description.clone()),
                        ..Default::default()
                    },
                };
                ReferenceOr::Item(Box::new(schema))
            }
        }
    };
    let gen = quote! {
        impl #name {
            pub fn ToMqttMsg<'a>(&self) -> MqttMsg {
                MqttMsg::new_s(
                    #topic_s,
                    #typ_s,
                    #act_s,
                    json!({
                        #(#field_names11: self.#field_names12),*
                    }),
                )
            }
            pub fn ToMqttMsgCustom<'a>(&self, topic: &'a str, typ: &'a str, act: &'a str) -> MqttMsg {
                MqttMsg::new_s(
                    topic,
                    typ,
                    act,
                    json!({
                        #(#field_names21: self.#field_names22),*
                    }),
                )
            }
            pub fn GetMessageName() -> String {
                #name2.to_owned()
            }
            pub fn GetMessage() -> Message {
                Message {
                    name: Some((#name2).to_owned()),
                    title: Some((#name2).to_owned()),
                    content_type: Some("application/json".to_owned()),
                    payload: Some(asyncapi::Payload::Schema(asyncapi::Schema {
                        schema_data: asyncapi::SchemaData {
                            ..Default::default()
                        },
                        schema_kind: asyncapi::SchemaKind::Type(asyncapi::schema::Type::Object(
                            ObjectType {
                                properties: IndexMap::from([
                                    (
                                        "a".to_owned(),
                                        ObjectType::create_propertie2(#act_s2.to_owned(), "action".to_owned())
                                    ),
                                    (
                                        "t".to_owned(),
                                        ObjectType::create_propertie2(#typ_s2.to_owned(), "type".to_owned())
                                    ),
                                    (
                                        "d".to_owned(),
                                        ReferenceOr::Item(Box::new(asyncapi::Schema {
                                            schema_data: asyncapi::SchemaData {
                                                ..Default::default()
                                            },
                                            schema_kind: asyncapi::SchemaKind::Type(
                                                asyncapi::schema::Type::Object(
                                                    ObjectType {
                                                        properties: IndexMap::from([
                                                            #((
                                                                stringify!(#field_names52).to_owned(),
                                                                #field_types4::create_propertie(stringify!(#field_types4).to_owned(), #examplev3.to_owned(), #descriptionv3.to_owned())
                                                            )),*
                                                        ]),
                                                        ..Default::default()
                                                    },
                                                ),
                                            ),
                                        })),
                                    ),
                                ]),
                                ..Default::default()
                            },
                        )),
                    })),
                    summary: Some("specs td all api".to_owned()),
                    ..Default::default()
                }
            }
            pub fn GetPayload() -> asyncapi::PayloadKind {
                asyncapi::PayloadKind {
                    name: Some((#name2).to_owned()),
                    description: Some(#description_s.to_owned()),
                    payload:
                    asyncapi::Schema {
                        schema_data: asyncapi::SchemaData {
                            ..Default::default()
                        },
                        schema_kind: asyncapi::SchemaKind::Type(asyncapi::schema::Type::Object(
                            ObjectType {
                            properties: IndexMap::from([
                                #((
                                    stringify!(#field_names32).to_owned(),
                                    #field_types5::create_propertie(stringify!(#field_types5).to_owned(), #examplev4.to_owned(), #descriptionv4.to_owned())
                                )),*
                            ]),
                            ..Default::default()
                        })),
                    }
                }
            }
            pub fn GetChannelName() -> String {
                #topic_s2.to_owned()
            }
            pub fn GetChannelData() -> Channel {
                Channel {
                    bindings: None,
                    description: Some((#name2).to_owned() + " " + #description_s),
                    parameters: IndexMap::from([(
                        "clientid".to_owned(),
                        asyncapi::ReferenceOr::Item(Parameter {
                            description: Some("clientid".to_owned()),
                            schema: Some(asyncapi::ReferenceOr::Item(asyncapi::Schema {
                                schema_data: asyncapi::SchemaData {
                                    ..Default::default()
                                },
                                schema_kind: asyncapi::SchemaKind::Type(asyncapi::schema::Type::String(
                                    StringType {
                                        ..Default::default()
                                    },
                                )),
                            })),
                            ..Default::default()
                        }),
                    )]),
                    publish: None,
                    reference: None,
                    servers: Vec::new(),
                    subscribe: Some(Operation {
                        description: None,
                        external_docs: None,
                        summary: None,
                        tags: Vec::new(),
                        traits: Vec::new(),
                        security: Vec::new(),
                        operation_id: Some(#typ_s3.to_owned() +"_" + #name2),
                        bindings: Some(asyncapi::ReferenceOr::Item(OperationBinding {
                            mqtt: Some(MQTTOperationBinding {
                                qos: Some(1),
                                ..Default::default()
                            }),
                            ..Default::default()
                        })),
                        message: Some(OperationMessageType::Single(
                            ReferenceOr::Item(Message {
                                same_channel_schema: Some(
                                    asyncapi::Schema {
                                        schema_data: asyncapi::SchemaData {
                                            ..Default::default()
                                        },
                                        schema_kind: asyncapi::SchemaKind::OneOf{
                                        one_of: vec![
                                            ReferenceOr::Item(
                                                asyncapi::PayloadKind {
                                                    name: Some((#name2).to_owned()),
                                                    description: Some(#description_s.to_owned()),
                                                    payload:
                                                    asyncapi::Schema {
                                                        schema_data: asyncapi::SchemaData {
                                                            ..Default::default()
                                                        },
                                                        schema_kind: asyncapi::SchemaKind::Type(
                                                            asyncapi::schema::Type::Object(
                                                                ObjectType {
                                                                    properties: IndexMap::from([
                                                                        (
                                                                            "a".to_owned(),
                                                                            ObjectType::create_propertie2(#act_s2.to_owned(), "action".to_owned())
                                                                        ),
                                                                        (
                                                                            "t".to_owned(),
                                                                            ObjectType::create_propertie2(#typ_s2.to_owned(), "type".to_owned())
                                                                        ),
                                                                        (
                                                                            "d".to_owned(),
                                                                            ReferenceOr::Item(Box::new(asyncapi::Schema {
                                                                                schema_data: asyncapi::SchemaData {
                                                                                    ..Default::default()
                                                                                },
                                                                                schema_kind: asyncapi::SchemaKind::Type(
                                                                                    asyncapi::schema::Type::Object(
                                                                                        ObjectType {
                                                                                            properties: IndexMap::from([
                                                                                                #((
                                                                                                    stringify!(#field_names42).to_owned(),
                                                                                                    #field_types3::create_propertie(stringify!(#field_types3).to_owned(), #examplev.to_owned(), #descriptionv.to_owned())
                                                                                                )),*
                                                                                            ]),
                                                                                            ..Default::default()
                                                                                        },
                                                                                    ),
                                                                                ),
                                                                            })),
                                                                        ),
                                                                    ]),
                                                                    ..Default::default()
                                                                }
                                                            )
                                                        ),
                                                    }
                                                }
                                            )
                                        ]
                                    }
                                }),
                                ..Default::default()
                                }),
                            )
                        ),
                        extensions: IndexMap::new(),
                    }),
                    extensions: IndexMap::new(),
                }
            }
        }
    };
    let mut res: TokenStream = TokenStream::new();
    if isEnum {
        res.extend(TokenStream::from(for_enum_gen));
    } else {
        res.extend(TokenStream::from(for_struct_gen));
    }
    res.extend(TokenStream::from(gen));
    res.into()
}
